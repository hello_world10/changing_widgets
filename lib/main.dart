import 'package:flutter/material.dart';

class Counter extends StatefulWidget {
  @override
  CounterState createState() => CounterState();
}

class CounterState extends State<StatefulWidget> {
  int counter = 0;
  void _increment() {
    setState(() {
      counter++;
    });
    print('Counter: $counter');
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ElevatedButton(onPressed: null, child: Text('Increment')),
        SizedBox(
          width: 16,
        ),
        Text('Counter: $counter'),
      ],
    );
  }
}

void main() {
  runApp(MaterialApp(
    home: Scaffold(
        body: Center(
      child: Counter(),
    )),
  ));
}
